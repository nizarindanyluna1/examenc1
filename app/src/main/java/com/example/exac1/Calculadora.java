package com.example.exac1;

public class Calculadora {

    private float num, num2;

    public Calculadora(float num, float num2) {
        this.num = num;
        this.num2 = num2;
    }

    public Calculadora (){
        this.num = 0.0f;
        this.num2 = 0.0f;
    }

    public float getNum() {
        return num;
    }

    public void setNum(float num) {
        this.num = num;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    public float suma(){
        return num + num2;
    }
    public float resta(){
        return num - num2;
    }
    public float multiplicacion(){
        return num * num2;
    }
    public float division(){
        return num / num2;
    }
}
