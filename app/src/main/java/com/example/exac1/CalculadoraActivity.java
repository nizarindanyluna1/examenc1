package com.example.exac1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {

    private Button btnLimpiar, btnSalir, btnSumar, btnRestar, btnMult, btnDivi;
    private EditText txtNumero1, txtNumero2;
    private TextView txtResultado, txtUsuario;
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();


        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");

        if (cliente != null) {
            txtUsuario.setText(cliente);
        } else {
            Toast.makeText(CalculadoraActivity.this, "Usuario no capturado", Toast.LENGTH_SHORT).show();
        }

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumero1.setText("");
                txtNumero2.setText("");
                txtResultado.setText("");
            }
        });

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("suma");
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("resta");
            }
        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("multiplicacion");
            }
        });

        btnDivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarOperacion("division");
            }
        });
    }

    private void realizarOperacion(String operacion) {
        try {
            float num1 = Float.parseFloat(txtNumero1.getText().toString());
            float num2 = Float.parseFloat(txtNumero2.getText().toString());

            calculadora.setNum(num1);
            calculadora.setNum2(num2);

            float resultado = 0;
            switch (operacion) {
                case "suma":
                    resultado = calculadora.suma();
                    break;
                case "resta":
                    resultado = calculadora.resta();
                    break;
                case "multiplicacion":
                    resultado = calculadora.multiplicacion();
                    break;
                case "division":
                    if (num2 == 0) {
                        Toast.makeText(this, "No se puede dividir entre 0", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    resultado = calculadora.division();
                    break;
            }
            txtResultado.setText(String.valueOf(resultado));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor ingrese números válidos", Toast.LENGTH_SHORT).show();
        }
    }

    private void iniciarComponentes() {
        txtNumero1 = findViewById(R.id.txtNumero1);
        txtNumero2 = findViewById(R.id.txtNumero2);
        txtResultado = findViewById(R.id.txtResultado);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMult = findViewById(R.id.btnMult);
        btnDivi = findViewById(R.id.btnDivi);
        txtUsuario = findViewById(R.id.txtUsuario);
        calculadora = new Calculadora();
    }
}